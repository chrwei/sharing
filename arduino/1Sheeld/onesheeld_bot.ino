#include <OneSheeld.h>
#include <Servo.h>
#include <RobotCore.h>

//SN754410 H-Bridge IC pins
const int motor1Pin1 = 2;    // 1A
const int motor1Pin2 = 4;    // 2A
const int motor2Pin1 = 7;    // 3A
const int motor2Pin2 = 8;    // 4A
const int enable1Pin = 9;    // 1,2 enable pin
const int enable2Pin = 10;    // 3,4 enable pin

RobotCore myBot;

int curPwr = 0;
int curTrn = 0;

void setup() {
  //start sheeld
  OneSheeld.begin();
  //config hbridge
  myBot.hbridge(motor1Pin1, motor1Pin2, enable1Pin, motor2Pin1, motor2Pin2, enable2Pin);
}

void loop() {
  if(abs(OrientationSensor.getZ()) > 5) { //ignore some wobble
    curPwr = map(OrientationSensor.getZ(), 90, -90, -255, 255); //map from degrees to PWM values
  } else {
    curPwr = 0;
  }
  //limit to max PWM
  if (curPwr < -255) curPwr = -255;
  if (curPwr > 255) curPwr = 255;

  if(abs(OrientationSensor.getY()) > 5) { //ignore some wobble
    if(abs(curPwr) > 60) { //limit turn speed when not going really slow
      curTrn = map(OrientationSensor.getY(), 90, -90, -90, 90); //map from degrees to PWM values
    } else {
      curTrn = map(OrientationSensor.getY(), 90, -90, -255, 255); //map from degrees to PWM values
    }
  } else {
    curTrn = 0;
  }
  //limit to max PWM
  if (curTrn < -255) curTrn = -255;
  if (curTrn > 255) curTrn = 255;
  
  //apply motor speed
  myBot.drive(curPwr, curTrn);
}

