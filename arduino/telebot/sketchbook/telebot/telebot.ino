/*
This is for the Sabertooth motor controller in Servo mode

serial protocol info:
inputs, no delimiters:
  M - motor commands
    1-9 - directions match keyboard numberpad directions, 5 is STOP
    Example:  "M7" - go forward and left
  P - program
    0 - manual drive
    1 - automatic
    2 - follow
  S - set speed.  does not take effect until the next motor move target is evaluated
    1 - full speed
    2 - 75%
    3 - 50%
    4 - 25%
  D - debug
    0 - none
    1 - ping sensor info
    Example: "D1" - enable ping debug

outputs, ":" delimited for everything, newline terminated
  I - information
    string - textual information
    Example: "I:done!" - startup done
  S - Stop info
    integer - sensor index (0-based)
    integer - distance cm
    Example: "S:2:71" - stop condition, 3rd sensor, at 71cm
  P - power info
    integer - left target
    integer - right target
    integer - left actual
    integer - right actual
    Example: "P:90:90:45:95" - target is 90/90 (stop) current is 45/135 (rotating in place, half speed)
  E - Error
    string - textual information
    Example: "E:Invalid Move" - input something not 1-9 after M
  D - debug info
    P - ping sensors
      integer - sensor index (0-based)
      integer - distance cm
      Example: "D:P:1:123" - debug ping, 2nd sensor at 123cm
    
*/
#include <SPI.h>  
#include <Pixy.h>
#include <Servo.h>
#include <RobotCore.h>
#include <NewPing.h>

#define MAX_DISTANCE 300 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.
#define MODE0_LIMIT 60      // max distance that it's OK to move
#define MODE1_TURN 114

#define X_CENTER    160L  //pixy center line

#define NUM_PINGS 4

enum SonarNames {
  ping0 = 0,
  ping45 = 1,
  ping90 = 2,
  ping135 = 3,
  ping180 = 4
};

NewPing sonar[NUM_PINGS] = {
  NewPing(25, 23, MAX_DISTANCE), // 60 degree
  NewPing(29, 27, MAX_DISTANCE), // 90 degree
  NewPing(33, 31, MAX_DISTANCE), // 120 degree
  NewPing(37, 35, MAX_DISTANCE) // 160 degree
};

uint8_t pingCurrent = 255; //255 means not pinging
volatile uint8_t pingNext = 255;
boolean pingDebug = false;
uint8_t lastTrigger = 255;
uint8_t maxPwr = 255;
uint8_t curPwr = 0;
uint8_t curDir = STOP;
uint8_t turnRatio = 0.5;
uint8_t roverMode = 0;
boolean powerEnable = false;

RobotCore bot;
Pixy pixy;

void setup() {
  Serial.begin(115200);
  Serial.println("I:starting...");

  bot.servo(44, 46, 1000, 2000);
  //prime the speed
  curPwr = maxPwr * 0.25;

  pingNext = 0;
  
  delay(500);
  Serial.println("I:done!");
}

void loop() 
{
  pingCheck();
  
  switch(roverMode) {
   case 0:
    checkMode0();
    break;
   case 1:
    checkMode1();
    break;
   case 2:
    checkMode2();
    break;
  }
  
  checkSerial();
  
  applyPower();
}

void echoCheck() { // timer's up, do next sensor
  if(sonar[pingCurrent].check_timer() != 0) {
    pingNext = pingCurrent + 1;
    if (pingNext >= NUM_PINGS)
      pingNext = 0;
  }
}

void pingCheck() {
  if(pingNext < NUM_PINGS) {
    if (pingCurrent < NUM_PINGS) {
      sonar[pingCurrent].timer_stop();
    }
    pingCurrent = pingNext;
    pingNext = 255;
    sonar[pingCurrent].ping_timer(echoCheck); // Do the ping (processing continues, interrupt will call echoCheck to look for echo).
  }
}

void checkMode0() { //simply halt if too close
  lastTrigger = 255;
  for (uint8_t i = 0; i < NUM_PINGS; i++) { // Loop through the sensors to see what's triggered
    if(pingDebug) {  //ping debug packet:  D:P:sensor index:distance cm
      Serial.print("D:P:"); Serial.print(i); Serial.print(":");Serial.println(sonar[i].ping_result / US_ROUNDTRIP_CM);
    }
    
    if (sonar[i].ping_result / US_ROUNDTRIP_CM > 0 && sonar[i].ping_result / US_ROUNDTRIP_CM < MODE0_LIMIT) {
      //S is e-stop, format is S:sensor index:distance cm
      Serial.print("S:"); Serial.print(i); Serial.print(":");Serial.println(sonar[i].ping_result / US_ROUNDTRIP_CM);
      switch(curDir) {
        case 7: 
        case 8:
        case 9: //only when going forward
          lastTrigger = i;
          powerEnable = false;
          bot.estop();
          break;
      }
      break; //only need one to be over
    }
  }
  if(lastTrigger == 255 && !powerEnable) { //it passed after a failure
    powerEnable = true;
    Serial.println("S::"); //empty estop packet to clear it
  }
}

void checkSerial() {
  if(Serial.available()) {            // Is data available from Internet
    char cmd;
    //keys match number pad directions
    cmd = Serial.read();
    switch(cmd) { //read 1st byte
      case 'M': //move
        curDir = Serial.read() - '0';  //read 2nd byte, convert char number to int
        switch(curDir) { //validate
          case BACK_LEFT:
          case BACK:
          case BACK_RIGHT:
          case SPIN_LEFT:
          case STOP:
            curPwr = 0;  //need to set speed for a stop
          case SPIN_RIGHT:
          case FWD_LEFT:
          case FWD:
          case FWD_RIGHT:
            break; //ok
          default:
            curDir = STOP;
            curPwr = 0;
            Serial.println("E:Invalid Move");
        }
        break;
      case 'P': //program
        switch(Serial.read()) { //read 2nd byte
          case '0': //drive
            roverMode = 0;
            turnRatio = 0.5;
            curDir = STOP;
            curPwr = 0;
            maxPwr = 255;
            break;
          case '1': //auto
            roverMode = 1;
            maxPwr = 255;
            curPwr = maxPwr * 0.25;
            pingDebug = true;
            break;
          case '2': //follow
            roverMode = 2;
            maxPwr = 255;
            curPwr = maxPwr * 0.25;
            pingDebug = true;
            break;
        }
        break;
      case 'S': //speed
        switch(Serial.read()) { //read 2nd byte
          case '0': //100%
            curPwr = maxPwr;
            break;
          case '1': //75%
            curPwr = maxPwr * 0.75;
            break;
          case '2': //50%
            curPwr = maxPwr * 0.50;
            break;
          case '3': //25%
            curPwr = maxPwr * 0.25;
            break;
        }
        break;
      case 'D': //debug?
        switch(Serial.read()) { //read 2nd byte
          case '0':
            pingDebug = false;
            break;
          case '1': //enable
            pingDebug = true;
            break;
        }
        break;
      default:
        Serial.print("E:Invalid command "); Serial.println(cmd);
        Serial.flush();
    }
  }
}

void applyPower() {
  if (powerEnable) {
    bot.dirSet(curDir, curPwr, 0.5);
  }
  bot.speedRampStep();
  //power info format: P:left target:right target:left actual:right actual
  Serial.print("P:"); Serial.print(bot.lPower(curPwr)); Serial.print(":"); Serial.print(bot.rPower(curPwr)); 
  Serial.print(":"); Serial.print(bot.lPower()); Serial.print(":"); Serial.println(bot.rPower()); 
}

void checkMode1() {
  boolean bAllClear = true;
  lastTrigger = 255;
  for (uint8_t i = 0; i < NUM_PINGS; i++) { // Loop through the sensors to see what's triggered
    if(pingDebug) {  //ping debug packet:  D:P:sensor index:distance cm
      Serial.print("D:P:"); Serial.print(i); Serial.print(":");Serial.println(sonar[i].ping_result / US_ROUNDTRIP_CM);
    }
    
    if (sonar[i].ping_result / US_ROUNDTRIP_CM > 0) {
      if(sonar[i].ping_result / US_ROUNDTRIP_CM < MODE0_LIMIT) {
        //gentle not working, evasive action!
        //spin until clear?
        switch(i) {
          case 0: //left side, turn right
          case 1: //left center, turn right
            Serial.println("I:Turn Right Hard");
            curDir = SPIN_RIGHT;
            bAllClear = false;
            break;
          case 2: //right center, turn left
          case 3: //right side, turn left
            Serial.println("I:Turn Left Hard");
            curDir = SPIN_LEFT;
            bAllClear = false;
            break;
        }
      } else if (bAllClear && sonar[i].ping_result / US_ROUNDTRIP_CM > 0 && sonar[i].ping_result / US_ROUNDTRIP_CM < MODE1_TURN) {
        //figure out what direction to gentle turn
        switch(i) {
          case 0: //left side, turn right
          case 1: //left center, turn right
            Serial.println("I:Turn Right");
            curDir = FWD_RIGHT;
            bAllClear = false;
            break;
          case 2: //right center, turn left
          case 3: //right side, turn left
            Serial.println("I:Turn Left");
            curDir = FWD_LEFT;
            bAllClear = false;
            break;
        }
        //map the turn ratio between the turning range to a 0 to 1 float, so it turns harder to closer to the spin limit
        turnRatio = map(sonar[i].ping_result / US_ROUNDTRIP_CM, MODE0_LIMIT, MODE1_TURN, 0, 100) / 100.0;
      } else if (bAllClear) {
       //clear, go forward
        Serial.println("I:Forward");
        curDir = FWD;
      }
    }
  }
}

void checkMode2() {
  boolean bAllClear = true;
  uint8_t distance = MAX_DISTANCE;
  uint16_t blocks;
  int8_t panError;
  uint16_t i=0, j;
  
  lastTrigger = 255;
  for (uint8_t i = 0; i < NUM_PINGS; i++) { // Loop through the sensors to see what's triggered
    if(pingDebug) {  //ping debug packet:  D:P:sensor index:distance cm
      Serial.print("D:P:"); Serial.print(i); Serial.print(":");Serial.println(sonar[i].ping_result / US_ROUNDTRIP_CM);
    }
    
    if (sonar[i].ping_result / US_ROUNDTRIP_CM > 0) {
      if (bAllClear && sonar[i].ping_result / US_ROUNDTRIP_CM > 0 && sonar[i].ping_result / US_ROUNDTRIP_CM < MODE0_LIMIT) {
        bAllClear = false;
        lastTrigger = i;
        powerEnable = false;
        bot.estop();
        Serial.println("I:Too Close");
      } 
    }
    if (distance > sonar[i].ping_result / US_ROUNDTRIP_CM) {
      distance = sonar[i].ping_result / US_ROUNDTRIP_CM;
    }
  }

  blocks = pixy.getBlocks();
  
  if (blocks)
  {
    //find biggest
    for (j=1; j<blocks; j++)
    {
      if (pixy.blocks[j].width > pixy.blocks[i].width) {
        i = j;
      }
    }

    panError = X_CENTER - (pixy.blocks[i].x + (pixy.blocks[i].width/2));
    
    if(distance > MODE0_LIMIT) {
      if(panError > -10 && panError < 10) { //near center, go forward
        curDir = FWD;
        if(distance > MODE1_TURN) {
          curPwr = maxPwr;
        } else {
          curPwr = map(distance, MODE0_LIMIT, MODE1_TURN, 0, maxPwr);
        }
      } else if(panError < 0) { //veer right, 
        if(panError < -100 || distance < MODE1_TURN) { //spin it
          curDir = SPIN_RIGHT;
        } else {
          curDir = FWD_RIGHT;
          curPwr = map(distance, MODE0_LIMIT, MODE1_TURN, 0, maxPwr);
          turnRatio = map(panError, 0, 0-X_CENTER, 0, 100) / 100.0;
        }
      } else { //veer left, slow right motor down
        if(panError > 100 || distance < MODE1_TURN) { //spin it
          curDir = SPIN_LEFT;
        } else {
          curDir = FWD_LEFT;
          curPwr = map(distance, MODE0_LIMIT, MODE1_TURN, 0, maxPwr);
          turnRatio = map(panError, 0, X_CENTER, 0, 100) / 100.0;
        }
      }
      powerEnable = true;
    }
  } else { //no blocks, stop
    curDir = STOP;
    curPwr = 0;
    powerEnable = true;
  }
  
}


