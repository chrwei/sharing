//	RobotCore.cpp - Library for robot essentials.
//	Created by Chris Weiss, May 2014.
//	Copyright (C) 2014  Chris Weiss	

#include "RobotCore.h"

void RobotCore::hbridge(uint8_t fPinR, uint8_t bPinR, uint8_t ePinR, uint8_t fPinL, uint8_t bPinL, uint8_t ePinL) {
	motorMode = 'H';
	
	fwdPinR = fPinR;
	bkdPinR = bPinR;
	enbPinR = ePinR;
	
	fwdPinL = fPinL;
	bkdPinL = bPinL;
	enbPinL = ePinL;
	
	pinMode (fwdPinR, OUTPUT); 
	pinMode (bkdPinR, OUTPUT); 
	pinMode (enbPinR, OUTPUT); 
	
	pinMode (fwdPinL, OUTPUT); 
	pinMode (bkdPinL, OUTPUT); 
	pinMode (enbPinL, OUTPUT); 
}

void RobotCore::servo(uint8_t Rpin, uint8_t Lpin) {
	motorMode = 'S';
	Rservo.attach(Rpin);
	Lservo.attach(Lpin);	
}
void RobotCore::servo(uint8_t Rpin, uint8_t Lpin, int min, int max) {
	motorMode = 'S';
	Rservo.attach(Rpin, min, max);
	Lservo.attach(Lpin, min, max);	
}

void RobotCore::speedRamp(boolean enable, uint8_t step) {
	spdRamp = enable;
	spdStep = step;
	spdCur = 0;
	spdTgt = 0;
}

void RobotCore::speedRampStep() {
	if (spdRamp && spdTgt != spdCur) {
		if(spdTgt < spdCur) {
			if(spdCur - spdTgt <= spdStep) { //final step
				spdCur = spdTgt;
			} else { //step down
				spdCur -= spdStep;
			}
		} else if (spdTgt > spdCur) {
			if(spdTgt - spdCur <= spdStep) { //final step
				spdCur = spdTgt;
			} else { //step up
				spdCur += spdStep;
			}
		}
	}
	speedSet();
}

void RobotCore::speedSet(uint8_t speed) {
	spdCur = speed;
	speedSet();
}
void RobotCore::speedSet() {
	uint8_t Rpower, Lpower;
	
	if (motorMode == 'H') {
		analogWrite(enbPinR, rPower());
		analogWrite(enbPinL, lPower());
	}
	//servo type gets power applied in motorSet
}

uint8_t RobotCore::rPower() {
	if (dirLast == 9 || dirLast == 3) { //right turn
		return spdCur * dirTurnRatio;
	} else {
		return spdCur;
	}
}

uint8_t RobotCore::lPower() {
	if (dirLast == 7 || dirLast == 1) { //right turn
		return spdCur * dirTurnRatio;
	} else {
		return spdCur;
	}
}

uint8_t RobotCore::rPower(uint8_t pwr) {
	if (dirLast == 9 || dirLast == 3) { //right turn
		return pwr * dirTurnRatio;
	} else {
		return pwr;
	}
}

uint8_t RobotCore::lPower(uint8_t pwr) {
	if (dirLast == 7 || dirLast == 1) { //right turn
		return pwr * dirTurnRatio;
	} else {
		return pwr;
	}
}

void RobotCore::dirSet(uint8_t dir, uint8_t speed, float tratio) {
	dirTurnRatio = tratio;
	if(spdRamp) {
		spdTgt = speed;
	} else {
		speedSet(speed);
	}
	
	if (dirLast != dir) {
		dirLast = dir;
		switch (dir) {
			case 1: //reverse modes, speed handles arc turns
			case 2:
			case 3:
				motorSet('R','B');
				motorSet('L','B');
				break;
			case 4: //left spin
				motorSet('R','F');
				motorSet('L','B');
				break;
			case 5: //stop
				//a normal stop comes with speed == 0
				break;
			case 6: //right spin
				motorSet('R','B');
				motorSet('L','F');
				break;
			case 7: //forward modes, speed handles arc turns
			case 8:
			case 9:
				motorSet('R','F');
				motorSet('L','F');
				break;
		}
	}
}

void RobotCore::motorSet(char mtr, char dir) {
	if (motorMode == 'H') {
		if        (mtr == 'R' && dir == 'F')  {
			digitalWrite (fwdPinR, HIGH); 
			digitalWrite (bkdPinR, LOW);
		} else if (mtr == 'R' && dir == 'B')  {
			digitalWrite (fwdPinR, LOW); 
			digitalWrite (bkdPinR, HIGH);
		} else if (mtr == 'L' && dir == 'F')  {
			digitalWrite (fwdPinL, HIGH); 
			digitalWrite (bkdPinL, LOW);
		} else if (mtr == 'L' && dir == 'B')  {
			digitalWrite (fwdPinL, LOW); 
			digitalWrite (bkdPinL, HIGH);
		}
	}
	if (motorMode == 'S') {
		if        (mtr == 'R' && dir == 'F')  {
			Rservo.write(map(rPower(), 0, 255, 90, 0)); //invert since 0=full speed and 90=stop
		} else if (mtr == 'R' && dir == 'B')  {
			Rservo.write(map(rPower(), 0, 255, 90, 180)); // 180=full speed and 90=stop
		} else if (mtr == 'L' && dir == 'F')  {
			Lservo.write(map(rPower(), 0, 255, 90, 0)); //invert since 0=full speed and 90=stop
		} else if (mtr == 'L' && dir == 'B')  {
			Lservo.write(map(rPower(), 0, 255, 90, 180)); // 180=full speed and 90=stop
		}
	}
}

void RobotCore::estop() {
	spdCur = 255;
	spdTgt = 0; //if ramping, power will be ramped to 0

	if (motorMode == 'H') { //full power drections motors effectively applies a brake
		digitalWrite (fwdPinR, HIGH); 
		digitalWrite (bkdPinR, HIGH);
		analogWrite(enbPinR, 255);
		analogWrite(enbPinL, 255);
	}	
	if (motorMode == 'S') {
		Rservo.write(90);
		Lservo.write(90);
	}
}
