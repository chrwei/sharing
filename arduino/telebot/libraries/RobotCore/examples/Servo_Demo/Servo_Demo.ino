#include <Servo.h>
#include <RobotCore.h>

const int motor1Pin = 9;    // right servo
const int motor2Pin = 10;    // left servo

RobotCore myBot;

void setup ()
{
  myBot.servo(motor1Pin, motor2Pin, 1000, 2000); //configure servo
}

void loop ()
{
  myBot.dirSet(8, 255, 0); //8=Forward, 255=full speed, 0=ratio, doens't apply for dir=8
  delay(1000);
  myBot.dirSet(5, 0, 0); //5=stop, 0=no power, 0=ratio, doens't apply for dir=5
  delay(1000); 
}
