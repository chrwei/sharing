#include <Servo.h>
#include <RobotCore.h>

const int motor1Pin1 = 3;    // 1A
const int motor1Pin2 = 5;    // 2A
const int motor2Pin1 = 6;    // 1B
const int motor2Pin2 = 9;    // 2B
const int enable1Pin = 10;    // A enable pin
const int enable2Pin = 11;    // B enable pin

RobotCore myBot;

void setup ()
{
  myBot.hbridge(motor1Pin1, motor1Pin2, enable1Pin, motor2Pin1, motor2Pin2, enable2Pin); //configure H-Bridge
}

void loop ()
{
  myBot.dirSet(8, 255, 0); //8=Forward, 255=full speed, 0=ratio, doens't apply for dir=8
  delay(1000);
  myBot.dirSet(5, 0, 0); //5=stop, 0=no power, 0=ratio, doens't apply for dir=5
  delay(1000); 
}
