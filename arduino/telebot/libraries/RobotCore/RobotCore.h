//	RobotCore.h - Library for robot essentials.
//	Created by Chris Weiss, May 2014.
//	Copyright (C) 2014  Chris Weiss	

#ifndef RobotCore_h
#define RobotCore_h

#include "Arduino.h"
#include "Servo.h"

//direction constants
#define BACK_LEFT	1
#define BACK		2
#define BACK_RIGHT	3
#define SPIN_LEFT	4
#define STOP		5
#define SPIN_RIGHT	6
#define FWD_LEFT	7
#define FWD			8
#define FWD_RIGHT	9

class RobotCore {

public:
	static const uint8_t Motor_LR = 0;
	static const uint8_t Motor_R = 1;
	static const uint8_t Motor_L = 2;
	
	void hbridge(uint8_t, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t);
	void servo(uint8_t, uint8_t);
	void servo(uint8_t, uint8_t, int, int);

	void speedRamp(boolean, uint8_t);
	void speedRampStep();
	void speedSet();
	void speedSet(uint8_t);
	uint8_t rPower(); //calculates actual right motor power
	uint8_t lPower(); //calculates actual left motor power
	uint8_t rPower(uint8_t); //calculates proposed right motor power
	uint8_t lPower(uint8_t); //calculates proposed left motor power
	void dirSet(uint8_t, uint8_t, float);
	void motorSet(char, char);
	void estop();
	
private:
	char motorMode; //'H' for hbridge, 'S' for servo

	boolean spdRamp; //ramping enabled
	uint8_t spdStep; //step per ramp
	uint8_t spdTgt;  //target speed
	uint8_t spdCur;  //current speed
	uint8_t dirLast; //numbers that match a PC 9-key keypad
	float dirTurnRatio; //ratio for arc turns
	
	uint8_t fwdPinR;
	uint8_t bkdPinR; 
	uint8_t enbPinR;

	uint8_t fwdPinL;
	uint8_t bkdPinL; 
	uint8_t enbPinL;

	Servo Rservo;
	Servo Lservo;

};

#endif
