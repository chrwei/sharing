RobotCore will (eventually) provide all the core things you'll need to 
make an arduino based robot using various commonparts.

Features:
- Generic H-Bridge support:  works with any H-Bridge that uses 3 control
	wires per motor.
- RC-Servo emulating speed controll support
- abstracts the motor code from the movement and speed logic.  same 
	movement code will work with servo or h-bridge bots with only 1 line
	of code change
- Speed ramp support:  set target speed and step and the bot will 
	smoothly ramp and down to the target speed

Documentation:

void hbridge(uint8_t fPinR, uint8_t bPinR, uint8_t ePinR, uint8_t fPinL, 
				uint8_t bPinL, uint8_t ePinL)
	setup H-Bridge pins
	fPinR = "forward" right motor
	bPinR = "backward" right motor
	ePinR = "enable" right motor - requires PWM pin for speed control
	fPinL = "forward" left motor
	bPinL = "backward" left motor
	ePinL = "enable" left motor - requires PWM pin for speed control

void servo(uint8_t Rpin, uint8_t Lpin, int min, int max)
	setup servo pins
	Rpin = right servo
	Lpin = left servo
	min = override Servo Library min pulse, optional
	max = override Servo Library max pulse, optional

void dirSet(uint8_t dir, uint8_t speed, float tratio) 
	set direction to travel
	dir = 1-9, number corrosponds with numberpad directions:
		1 = back left
		2 = back
		3 = back right
		4 = spin counter clockwise
		5 = stop
		6 = spin clockwise
		7 = forward left
		8 = forward
		9 = forward right 
	speed = 0-255, 0 means stop, 255 means full power
	tratio = 0.0-1.0 turn speed ratio
		dir's 1,3,7,9 will slow down one wheel to make an arc.  
		set this to 0.5 will be half power on that wheel.  the smaller 
		the number the tighter the turn. 

void speedRamp(boolean enable, uint8_t step) 
	speed ramping
	enable = true/false, true to enable
	step = speed value change amount per call to speedRampStep()
	
void speedRampStep()
	perform the next speed ramp step

void speedSet(uint8_t speed) 
	manually set a speed, normally don't need to use this and
		speedRampStep() and dirSet() will override it
	speed = 0-255, 0 means stop, 255 means full power

void motorSet(char mtr, char dir) 
	manually set a motor on, normally don't need to use this and
		dirSet() will override it
	mtr = 'R' for right motor or 'L' for left motor
	dir = 'F' for forward or 'B' for backward. 
