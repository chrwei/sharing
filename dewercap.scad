difference(){
	cylinder(h=30 , r=40);
	union() {
		translate([0, 0, 5]) {
			cylinder(h=30 , r=35);
		}
		translate([-37.5, -2.5, 5]) {
			cube([75, 5, 30]);
		}
		translate([-2.5, -37.5, 5]) {
			cube([5, 75, 30]);
		}
	}
}