$fn=360;
$topdia=48;
$botdia=40;
$screwrad=1.25;
$height=30;

module torus(outerRadius, innerRadius)
{
  r=(outerRadius-innerRadius)/2;
  rotate_extrude() translate([innerRadius+r,0,0]) circle(r,$fn=90);	
}
module outside_circular_radius(outerRadius, roundingRadius)
  {
  translate([0,0,-roundingRadius])
    rotate_extrude()
	  translate([outerRadius-roundingRadius,0,0])
        difference()
          {
	      square(roundingRadius+0.1);
	      circle(roundingRadius,$fn=60);
	      }
  }

  //build platform for debug
//  color([0.5,0.5,0.5,0.1]) translate([-50,-50,-1.01]) %cube([100,100,1]);

 //#translate([0,0,3]) torus(outerRadius=(30+6)/2,innerRadius=(30-6)/2);


difference()
{
  cylinder(r=$topdia/2,h=$height);
  translate([0,0,$height + 65]) sphere($topdia+20, $fa=5, $fs=.01); //$fn=100
  union()
  {
    translate([0,0,$height/2]) torus(outerRadius=($topdia+11+11)/2,innerRadius=($topdia-4-4)/2);
    difference()
    {
      translate([0,0,-0.1]) cylinder(r=($topdia/2)+0.2,h=($height/2)+0.1);
      translate([0,0,-0.1]) cylinder(r=$botdia/2,h=($height/2)+0.2);
    }
  }
   translate([0,0,$height/2]) torus(outerRadius=($topdia+11+11)/2,innerRadius=($topdia-4-4)/2);
   translate([0,0,$height]) outside_circular_radius(outerRadius=$topdia/2,roundingRadius=$height/10); 
   difference()
     {
     translate([0,0,$height*0.325483333]) outside_circular_radius(outerRadius=$topdia/2,roundingRadius=$height/10);  
     cylinder(r=$topdia*0.95625/2,h=($height/3)*2);
     }

   mirror([0,0,1])
     difference()
       {
       translate([0,0,($height*0.325483333)-$height]) outside_circular_radius(outerRadius=$topdia/2,roundingRadius=$height/10);  
       translate([0,0,-$height]) cylinder(r=$topdia*0.95625/2,h=($height/3)*2);
       }

   translate([0,0,-0.1]) cylinder(r=($botdia/2)-2,h=1);
   translate([0,0,-1]) cylinder(r=$screwrad,h=($height/3)*2);
}

