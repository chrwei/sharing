#!/bin/bash
pwd
if [ ! -f nomkv ] ; then
	for file in *.avi; do
		if [ -f "$file" ]; then
			new=`echo $file | sed 's/.avi$/.mkv/'`
			HandBrakeCLI -i "$file" -o "$new" -e x264  -q 20.0 -a 1 -E ffaac -B 160 -6 dpl2 -R Auto -D 0.0 -f mkv --loose-anamorphic -m -x ref=2:bframes=2:subme=6:mixed-refs=0:weightb=0:8x8dct=0:trellis=0
			rm "$file"
		fi
	done
fi
if [ -f domp4 ] ; then
	for file in *.mp4; do
		if [ -f "$file" ]; then
			new=`echo $file | sed 's/.mp4$/.mkv/'`
			HandBrakeCLI -i "$file" -o "$new" -e x264  -q 20.0 -a 1 -E faac -B 160 -6 dpl2 -R Auto -D 0.0 -f mkv --loose-anamorphic -m -x ref=2:bframes=2:subme=6:mixed-refs=0:weightb=0:8x8dct=0:trellis=0
			rm "$file"
		fi
	done
fi
