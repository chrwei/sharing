#!/bin/sh

IFS=$"
"
for dir in `find . -type d -regextype posix-egrep -iregex "./[^/]*/S(eason|eries)[[:space:]]+[0-9]+"`; do
	(cd "$dir"; tvrenamer.pl ; )
done
