module spacer(height) {
	union() {
		difference() {
			union () {
				translate ([0, 6.5, height/2]) {
					cube([43, 8, height], center = true);
				}
				translate ([0, -3.5, height/2]) {
					cube([8, 15.5, height], center = true);
				}
				translate ([0, -15, height/2]) {
					cube([28, 8, height], center = true);
				}
			}
			translate ([16.66875, 6.5, height/2]) {
				cylinder(height+0.1, 2.38125, 2.38125, center = true);
			}
			translate ([-16.66875, 6.5, height/2]) {
				cylinder(height+0.1, 2.38125, 2.38125, center = true);
			}
		}
	}
}

spacer(
12.7
);

mirror([0,1,0]) {
	translate([23, 19, 0]) {
		spacer(
15.875
);
	}
}

mirror([0,1,0]) {
	translate([-23, 19, 0]) {
		spacer(
9.525
);
	}
}

mirror([0,1,0]) {
	translate([0, -23, 0]) {
		spacer(
9.525
);
	}
}

translate([23, 42, 0]) {
	spacer(
9.525
);
}

