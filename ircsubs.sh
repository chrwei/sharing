#!/bin/bash

# our variables
BMOVL="/tmp/bmovl"
CTL="/tmp/ctl"
MYNICK="arsubs"
MYCHAN="#archreactor"
SUBFILE="/tmp/sub.tga"

# setup our sockets
[ ! -e "$BMOVL" ] && mkfifo "$BMOVL"
[ ! -e "$CTL" ] && mkfifo "$CTL"

playing="false"
mplayerpid="0"
nicknum=0
ourfile=""

# open our irc filehandle on #3
exec 3<>/dev/tcp/chat.freenode.net/6667
# send our connect string
echo "nick $MYNICK">&3
echo "user $MYNICK \"\" \"\" :$MYNICK">&3
# loop while the socket's open
while read aline <&3; do
	echo "$aline"
	# figure out our irc command
	CMD=`echo $aline | awk '{print $2}'`
	# if it signals we've connected, then join a channel
	if [ $CMD = "001" ]; then
		echo "join $MYCHAN">&3
	#if nick taken
	elif [ $CMD = "433" ]; then
		let $nicknum=$nicknum+1
		MYNICK=$MYNICK$nicknum
		echo "nick is now $MYNICK"
		echo "nick $MYNICK">&3
		echo "user $MYNICK \"\" \"\" :$MYNICK">&3

	# if it's a ping, reply
	elif [ $CMD = "PING" ]; then
		echo $aline | sed 's/PING/PONG/' >&3
	# if it is a private message
	elif [ $CMD = "PRIVMSG" ]; then
		# figure out our nick and message
		nick=$(echo "$aline" | sed -e 's/^://;s/!.*//')
		# FIXME \" fails
		msg=$(echo "$aline" | sed -e 's/^://;s/^[^:]*://;s/\r$//;s/"/\\\"/g')
		# if it's a CTCP or something fishy, skip it
		[ $(echo "$msg" | grep -c "\x01") -gt 0 ] && continue
		# FIXME add in proper CTCP support
		# print our line to the console
		echo "$nick: $msg"
		# if our message is the magic word
		if [ `echo $msg | awk '{print $1}'` = "SETUP" ]; then
			ourfile=`echo $msg | awk '{print $2}'`
			echo "PRIVMSG $MYCHAN :playing $ourfile">&3
			# get our res from our file
			res=$(mplayer -vo null -ao null -identify -frames 0 "$ourfile" | awk '/VIDEO:/ {print $3}')
			width=$(echo "$res" | awk -Fx '{print $1}')
			height=$(echo "$res" | awk -Fx '{print $2}')

			# start our movie playing
			mplayer -vf bmovl=0:0:"$BMOVL" "$ourfile" < "$CTL" &
			mplayerpid=$!
			echo "mplayerpid: $mplayerpid"
			# pause our file
			echo -n " " > "$CTL"
			echo "PRIVMSG $MYCHAN :Setup $ourfile ready for START" >&3
			playing="false"
		elif [ "$msg" == "START" ]; then
			if [ "$playing" = "false" ]; then
				echo "PRIVMSG $MYCHAN :Starting $ourfile">&3
				# start playing
				echo -n " " > "$CTL"
				playing="true"
			else
				# if we're not playing, no need to do anything else
				continue
			fi
		elif [ "$msg" = "PAUSE" ]; then
			echo "PRIVMSG $MYCHAN :Pausing $ourfile">&3
			# pausing playing
			echo -n " " > "$CTL"
			playing="false"
		elif [ "$msg" = "STOP" ]; then
			echo "PRIVMSG $MYCHAN :Stopping $ourfile">&3
			# stop playing
			echo -n "q" > "$CTL"
			kill -f $mplayerpid
			playing="false"
		else
			# shfit our lines up
			line3="$line2"
			line2="$line1"
			line1="$nick: $msg"
			#only make subs if playing and the pid is still alive
			if [ $playing = "true" ]; then
				if [ "`ps aux | awk '{print $2}' | grep $mplayerpid`" = "$mplayerpid" ]; then
					# generate our subtitle image
					convert -size ${width}x58 xc:transparent -channel RGBA +antialias -background black -pointsize 18 -font Helvetica \
					-fill white -draw "text 0,18 \"$line3\"" \
					-draw "text 0,36 \"$line2\"" \
					-draw "text 0,54 \"$line1\"" \
					-flip tga:- > "$SUBFILE"
					# tell mplayer how to display our file
					echo "RGBA32 $width 58 0 $[ $height - 58 ] 128 0" > $BMOVL
					cat "$SUBFILE" > $BMOVL
					echo -n "SHOW" > $BMOVL
				fi
			fi
		fi
	fi
done
